name := "issue-staleness-checker"

scalacOptions ++= Seq("-Xfatal-warnings", "-feature", "-language:higherKinds", "-language:reflectiveCalls")

scalaVersion := "2.10.1"

testOptions in Test += Tests.Argument("console", "junitxml")

initialCommands in console := "import scalaz._, Scalaz._"
