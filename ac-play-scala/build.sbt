import play.PlayImport._
import sbtrelease.ReleasePlugin._
import sbtrelease.Version

name := "ac-play-scala"

organization := "com.atlassian.connect"

scalaVersion := "2.11.5"

crossScalaVersions := Seq("2.10.4", "2.11.5")

javacOptions ++= Seq("-source", "1.6", "-target", "1.6")

// Play 2.3 deprecated WSRequest.setHeader(String, String) method that we need but provided no alternatives.
// So we have to temporarily disable fatal-warnings.
// scalacOptions ++= Seq("-Xfatal-warnings", "-feature", "-language:higherKinds", "-target:jvm-1.6")
scalacOptions ++= Seq("-feature", "-language:higherKinds", "-target:jvm-1.6")

val atlassianJwtVersion = "1.2.4"

val playDep = Seq(
    ws
  , filters
)

val slickDep = Seq(
    "com.typesafe.slick" %% "slick" % "2.1.0"
  , "com.typesafe.play" %% "play-slick" % "0.8.1"
  , "postgresql" % "postgresql" % "8.4-701.jdbc3"
)

val jwtDep = Seq(
    "com.atlassian.jwt" % "jwt-api" % atlassianJwtVersion
  , "com.atlassian.jwt" % "jwt-core" % atlassianJwtVersion
  , "commons-lang" % "commons-lang" % "2.6" // used by jwt
)

val libDependencies = Seq(
    "commons-codec" % "commons-codec" % "1.10"
  , "commons-fileupload" % "commons-fileupload" % "1.3.1"
  , "org.scalaz" %% "scalaz-core" % "7.0.6" // scalaz 7.1.0 is only compatible with play 2.4.x or above
)

val sharedTestDependencies = Seq(
    "junit" % "junit" % "4.12"
  , "org.specs2" %% "specs2" % "2.3.13" // specs2 2.4.x depends on scalaz-7.1.0
  , "org.mockito" % "mockito-all" % "1.10.19"
  , "com.typesafe.akka" %% "akka-testkit" % "2.3.9"
)

testOptions in Test += Tests.Argument("console", "junitxml")

libraryDependencies ++= (playDep ++ slickDep ++ jwtDep ++ libDependencies) ++ (sharedTestDependencies map (_ % "test"))

// credentials required when accessing atlassian-proxy-internal
credentials := Seq(Credentials(Path.userHome / ".ivy2" / ".credentials"))

resolvers ++= Seq(
  Resolver.defaultLocal,
  "atlassian-proxy-internal" at "https://m2proxy.atlassian.com/content/groups/internal/",
  "atlassian-proxy-public" at "https://m2proxy.atlassian.com/content/groups/public/",
  "atlassian-maven-public" at "http://maven.atlassian.com/content/groups/public/",
  Classpaths.typesafeReleases,
  DefaultMavenRepository,
  Resolver.sonatypeRepo("releases"),
  Resolver.mavenLocal
)

mappings in(Compile, packageBin) <+= baseDirectory map { base =>
  (base / "LICENSE.txt") -> "META-INF/LICENSE.txt"
}

packagedArtifacts += ((artifact in PlayKeys.playPackageAssets).value -> PlayKeys.playPackageAssets.value)

releaseSettings

ReleaseKeys.crossBuild := true

ReleaseKeys.versionBump := Version.Bump.Bugfix

publishTo <<= version { (v: String) =>
  val repo = "https://maven.atlassian.com/"
  if (v.trim.endsWith("SNAPSHOT"))
    Some("Atlassian Repositories" at repo + "content/repositories/atlassian-public-snapshot")
  else
    Some("Atlassian" at repo + "content/repositories/atlassian-public")
}

publishMavenStyle := true

pomExtra :=
  <organization>
    <name>Atlassian</name>
    <url>http://www.atlassian.com/</url>
  </organization>
  <url>https://bitbucket.org/atlassianlabs/atlassian-connect-play-scala</url>
  <licenses>
    <license>
      <name>Apache License 2.0</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0</url>
      <distribution>repo</distribution>
    </license>
  </licenses>
  <scm>
    <url>https://bitbucket.org/atlassianlabs/atlassian-connect-play-scala/src</url>
    <connection>scm:git:git@bitbucket.org:atlassianlabs/atlassian-connect-play-scala.git</connection>
    <developerConnection>scm:git:git@bitbucket.org:atlassianlabs/atlassian-connect-play-scala.git</developerConnection>
  </scm>

scalariformSettings

net.virtualvoid.sbt.graph.Plugin.graphSettings

lazy val main = (project in file(".")).enablePlugins(play.PlayScala)
