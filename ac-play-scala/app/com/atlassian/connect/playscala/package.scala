package com.atlassian.connect

package object playscala {

  implicit final class OptionOps[A](val op: Option[A]) extends AnyVal {

    def ifOnly(b: Boolean): Option[A] = if (b) op else None

  }
}
