package com.atlassian.connect.playscala.auth.jwt

import com.atlassian.connect.playscala.Constants
import play.api.libs.ws.{ WS, WSResponse, WSRequestHolder }
import com.atlassian.connect.playscala.auth.Token
import scala.concurrent.Future
import play.api.http.{ ContentTypeOf, Writeable }
import Constants._

trait JwtSignedAcHostWS extends JwtConfig {

  class SignedRequestHolder(val reqHolder: WSRequestHolder) {
    def signedGet()(implicit token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signed.get()
    }

    def signedPost[T](body: T)(implicit wrt: Writeable[T], ct: ContentTypeOf[T], token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signed.post(body)(wrt, ct)
    }

    def signedPut[T](body: T)(implicit wrt: Writeable[T], ct: ContentTypeOf[T], token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signed.put(body)(wrt, ct)
    }

    def signedDelete()(implicit token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signed.delete()
    }

    def signedHead()(implicit token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signed.head()
    }

    def signedOptions()(implicit token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signed.options()
    }

    def signed(implicit token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): WSRequestHolder = {
      val req = token.user.map(userKey => reqHolder.withQueryString(AC_USER_ID_PARAM -> userKey)).getOrElse(reqHolder)
        // because we need to sign again in those cases.
        .withFollowRedirects(follow = false)
      req.sign(new JwtSignatureCalculator(jwtAuthorizationGenerator, token.acHost, token.user, req.queryString))
    }
  }

  import scala.language.implicitConversions
  implicit def toSignedRequestHolder(reqHolder: WSRequestHolder) = new SignedRequestHolder(reqHolder)

}
