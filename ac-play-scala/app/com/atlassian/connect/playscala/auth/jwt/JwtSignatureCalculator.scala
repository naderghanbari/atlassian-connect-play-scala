package com.atlassian.connect.playscala.auth.jwt

import play.api.libs.ws.{ WSSignatureCalculator, WSRequest }
import java.lang.String._
import play.Logger
import com.atlassian.connect.playscala.model.AcHostModel

class JwtSignatureCalculator(val jwtAuthorizationGenerator: JwtAuthorizationGenerator, acHost: AcHostModel, userId: Option[String], queryParams: Map[String, Seq[String]] = Map()) extends WSSignatureCalculator {

  def sign(request: WSRequest): Unit = {
    getAuthorizationHeaderValue(request) foreach {
      authorizationHeaderValue =>
        Logger.debug(format("Generated Jwt authorisation header: '%s'", authorizationHeaderValue))
        // TODO
        // WSRequest.setHeader(String, String) has been deprecated since Play 2.3 but there is no other way to sign
        // a request without calling this method. Play WS's own OAuthCalculator calls this deprecated method.
        // We have to use this method and thus turn fatal-warnings off in built.sbt.
        // Future Play WS may change the signature of the sign method to return a new WSRequest (now known as WSRequestHolder).
        // See https://groups.google.com/forum/#!topic/play-framework/CUUxmgh0B2A
        request.setHeader("Authorization", authorizationHeaderValue)
    }
  }

  private def getAuthorizationHeaderValue(request: WSRequest): Option[String] =
    /*
      DO NOT call request.queryString here
      https://github.com/playframework/playframework/issues/2336
	   */
    jwtAuthorizationGenerator.generate(request.method, request.url, queryParams, acHost, userId)
}
