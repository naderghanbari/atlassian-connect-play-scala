package com.atlassian.connect.playscala.auth.jwt

import com.atlassian.jwt.core.http.{ JwtRequestExtractor, HttpRequestWrapper, AbstractJwtRequestExtractor }
import play.api.Logger
import play.api.mvc.{ SimpleResult, Results, RequestHeader }
import com.atlassian.jwt.{ Jwt, CanonicalHttpRequest }
import java.util
import scala.collection.JavaConverters._
import java.lang.Iterable
import com.atlassian.jwt.core.http.auth.{ SimplePrincipal, AuthenticationResultHandler, AbstractJwtAuthenticator }
import play.mvc.Http.Response
import com.atlassian.jwt.reader.{ JwtClaimVerifier, JwtReaderFactory }
import java.security.Principal
import play.api.http.Status
import com.atlassian.jwt.JwtConstants.HttpRequests.{ ADD_ON_ID_ATTRIBUTE_NAME, JWT_SUBJECT_ATTRIBUTE_NAME }

import scalaz.{ \/-, -\/ }

class PlayRequestWrapper(val request: RequestHeader) extends HttpRequestWrapper {
  override def getCanonicalHttpRequest: CanonicalHttpRequest = new CanonicalHttpRequest {
    override def getParameterMap: util.Map[String, Array[String]] = request.queryString.mapValues(_.toArray).asJava

    override def getMethod: String = request.method

    override def getRelativePath: String = request.path
  }

  override def getHeaderValues(headerName: String): Iterable[String] = request.headers.getAll(headerName).toIterable.asJava

  override def getParameter(parameterName: String): String = request.getQueryString(parameterName).orNull
}

class PlayJwtRequestExtractor extends AbstractJwtRequestExtractor[RequestHeader] {
  override def wrapRequest(request: RequestHeader): HttpRequestWrapper = new PlayRequestWrapper(request)
}

class PlayJwtAuthenticator(val jwtExtractor: JwtRequestExtractor[RequestHeader],
    val authenticationResultHandler: AuthenticationResultHandler[Response, JwtAuthenticationResult],
    val jwtReaderFactory: JwtReaderFactory) extends AbstractJwtAuthenticator[RequestHeader, Response, JwtAuthenticationResult](jwtExtractor, authenticationResultHandler) {

  protected def authenticate(request: RequestHeader, jwt: Jwt): Principal =
    if (jwt.getSubject == null) null else new SimplePrincipal(jwt.getSubject)

  protected def verifyJwt(jwt: String, claimVerifiers: util.Map[String, _ <: JwtClaimVerifier]): Jwt = {
    jwtReaderFactory.getReader(jwt).readAndVerify(jwt, claimVerifiers)
  }

  override def tagRequest(request: RequestHeader, jwt: Jwt): Unit = {
    //request.setAttribute(ADD_ON_ID_ATTRIBUTE_NAME, jwt.getIssuer());
    //request.setAttribute(JWT_SUBJECT_ATTRIBUTE_NAME, jwt.getSubject());
  }

}

class PlayAuthenticationResultHandler extends AuthenticationResultHandler[Response, JwtAuthenticationResult] {
  override def success(message: String, principal: Principal, authenticatedJwt: Jwt): JwtAuthenticationResult = \/-(authenticatedJwt)

  override def createAndSendForbiddenError(e: Exception, response: Response): JwtAuthenticationResult = error(e, Status.FORBIDDEN, "Access to this resource is forbidden without successful authentication. Please supply valid credentials.")

  override def createAndSendUnauthorisedFailure(e: Exception, response: Response, externallyVisibleMessage: String): JwtAuthenticationResult = error(e, Status.UNAUTHORIZED, externallyVisibleMessage)

  override def createAndSendBadRequestError(e: Exception, response: Response, externallyVisibleMessage: String): JwtAuthenticationResult = error(e, Status.BAD_REQUEST, externallyVisibleMessage)

  override def createAndSendInternalError(e: Exception, response: Response, externallyVisibleMessage: String): JwtAuthenticationResult = error(e, Status.INTERNAL_SERVER_ERROR, externallyVisibleMessage)

  private def error(e: Exception, status: Int, message: String) = {
    Logger.error(s"JWT Verification Error ($message), $status", e)
    -\/(Results.Status(status))
  } // TODO how to specify a body?
}