package com.atlassian.connect.playscala
package auth

import com.atlassian.connect.playscala.model.AcHostModel
import org.apache.commons.codec.binary.Base64
import play.api.libs.Crypto
import play.api.libs.json.{ Writes, Reads, Json }
import play.api.Logger
import scalaz.syntax.std.option._

case class Token(acHost: AcHostModel, user: Option[String], allowInsecurePolling: Boolean = false, timestamp: Long = System.currentTimeMillis()) {
  lazy val encryptedToken: String = {
    import Token._
    val compressed = CompressedToken(acHost.key.value, user, 1.some ifOnly allowInsecurePolling, timestamp)
    val jsonToken = Base64 encodeBase64String Json.toJson(compressed).toString().getBytes
    val result = Crypto encryptAES jsonToken
    Logger.debug(s"Encrypted $compressed to $result")
    result
  }
}

case class CompressedToken(h: String, u: Option[String], p: Option[Int], t: Long)

object Token {

  val PageTokenParam: String = "acpt"
  val PageTokenHeader: String = "X-" + PageTokenParam

  implicit val compressTokenReads: Reads[CompressedToken] = Json.reads[CompressedToken]
  implicit val compressTokenWrites: Writes[CompressedToken] = Json.writes[CompressedToken]
}

