package com.atlassian.connect.playscala.controllers

import play.api.mvc._
import scala.concurrent.{ ExecutionContext, Future }
import play.api.mvc.Result

private[controllers] trait ActionWrapper extends ActionBuilder[Request] {

  final def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]) =
    run(block)(this.executionContext)(request)

  final def apply[A](delegate: Action[A]): Action[A] = new Action[A] {
    def parser = delegate.parser
    def apply(request: Request[A]) = run(delegate.apply)(delegate.executionContext)(request)
  }

  def run[A](delegate: Request[A] => Future[Result])(implicit ec: ExecutionContext): Request[A] => Future[Result]

}
