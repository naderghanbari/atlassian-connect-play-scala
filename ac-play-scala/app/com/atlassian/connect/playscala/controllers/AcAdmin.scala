package com.atlassian.connect.playscala.controllers

import com.atlassian.connect.playscala.{ AcConfigured, PlayAcConfigured }
import play.api.mvc.{ Action, Controller }
import com.atlassian.connect.playscala.model.{ ClientKey, AcHostModel }
import com.atlassian.connect.playscala.util.AcHostWS.uri
import com.atlassian.connect.playscala.auth.Token
import play.api.libs.json.Json
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits._
import scalaz.syntax.std.option._
import com.atlassian.connect.playscala.store.DbConfiguration
import com.atlassian.connect.playscala.auth.jwt.JwtSignedAcHostWS

trait AcAdmin extends Controller with ActionPredicates with JwtSignedAcHostWS with DbConfiguration with AcConfigured {

  import play.api.Play.current

  def index() = Action { implicit request =>
    OnlyInDevMode {
      val all: Seq[AcHostModel] = acHostModelStore.all()
      Ok(views.html.ac.internal.admin.home(all))
    }
  }

  def clearMacroCache(key: String) = Action.async { implicit request =>
    OnlyInDevMode {
      def message(title: String, message: String) = Json.obj(
        "title" -> title,
        "message" -> message
      )
      acHostModelStore.findByKey(ClientKey(key)).fold(Future(BadRequest(message("No AC host", "Couldn't find AC host to send request to.")))) { acHostModel =>
        // use a hard-coded user-id "admin", which is usually available in dev mode in atlassian products
        // may need to resurrect acDelete() if the sign() method is still broken from the library
        val future = uri(acHostModel, s"/rest/atlassian-connect/1/macro/app/" + acConfig.pluginKey).signedDelete()(Token(acHostModel, "admin".some), jwtAuthorizationGenerator)
        future map {
          case resp if resp.status == play.api.http.Status.NO_CONTENT =>
            Ok(message("Cache cleared", s"The macro cache for host at '${acHostModel.baseUrl}' was cleared."))
          case resp => BadRequest(message("Unknown error",
            s"An unknown error happened clearing the cache for host at '${acHostModel.baseUrl}'. Http status is ${resp.status}} (${resp.statusText}})."))
        }
      }
    }
  }
}

import com.atlassian.connect.playscala.store.DefaultDbConfiguration

object AcAdmin extends AcAdmin with DefaultDbConfiguration with PlayAcConfigured
