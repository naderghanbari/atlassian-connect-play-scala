package com.atlassian.connect.playscala

import play.api.Play

import com.atlassian.connect.playscala.Constants._

import scala.sys.BooleanProp

trait PlayAcConfigured extends AcConfigured {
  import com.atlassian.connect.playscala.{ AC => ACObject }
  lazy val acConfig = ACObject
}

object AC extends AcConfig {

  import scala.concurrent.duration._
  import Play.current

  lazy val isDev = Play.isDev || Play.isTest || Play.configuration.getBoolean(AC_DEV).getOrElse(false) || BooleanProp.valueIsTrue(AC_DEV)

  lazy val pluginKey = Play.configuration.getString(AC_PLUGIN_KEY).getOrElse(
    if (isDev) "_add-on_key" else throw new IllegalStateException("ac.key not configured."))

  lazy val pluginName = Play.configuration.getString(AC_PLUGIN_NAME).getOrElse(
    if (isDev) "Atlassian Connect Play Add-on" else pluginKey)

  lazy val pluginVersion: Option[String] = Play.configuration.getString(AC_PLUGIN_VERSION).orElse(
    if (isDev) Some("dev") else None)

  lazy val baseUrl: String = current.configuration.getString("application.baseUrl") getOrElse "http://localhost:9000"

  lazy val tokenExpiryLimit: FiniteDuration =
    current.configuration.getLong(Constants.AC_TOKEN_EXPIRY).fold(15 minutes span)(_ seconds span)
}
