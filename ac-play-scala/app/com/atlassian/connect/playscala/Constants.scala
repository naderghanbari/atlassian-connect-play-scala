package com.atlassian.connect.playscala

object Constants {
  val AC_TOKEN_EXPIRY: String = "ac.token.expiry.secs"
  val AC_DEV: String = "ac.dev"
  val AC_PLUGIN_KEY: String = "ac.key"
  val AC_PLUGIN_NAME: String = "ac.name"
  val AC_PLUGIN_VERSION: String = "ac.version"

  // must not use '.' in the config key if environment variable lookup is needed, as it will cause typesafe config to complain:
  // com.typesafe.config.ConfigException$WrongType: env var OAUTH_LOCAL_PUBLIC_KEY: ac.oauth.local.public_key has type STRING rather than OBJECT
  val AC_OAUTH_LOCAL_PUBLIC_KEY_FILE: String = "ac_oauth_local_public_key_file"
  val AC_OAUTH_LOCAL_PRIVATE_KEY_FILE: String = "ac_oauth_local_private_key_file"
  val AC_OAUTH_LOCAL_PUBLIC_KEY: String = "ac_oauth_local_public_key"
  val AC_OAUTH_LOCAL_PRIVATE_KEY: String = "ac_oauth_local_private_key"

  val AC_USER_ID_PARAM: String = "user_id"
  val AC_HOST_PARAM: String = "ac_host"
  val AC_TOKEN: String = "ac_token"
}
